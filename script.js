class GameField {

    constructor() {
        this.whoWalks = 'x'
        this.isOverGame = false
    }

    #state = [[null, null, null],
             [null, null, null],
             [null, null, null]]
    
    static whoWalks
    static isOverGame

    getGameFieldStatus() {
        let fieldStr = '\n    0  1  2\n'
        for(let i = 0; i < 3; i++)
        {
            fieldStr = fieldStr + i + '   '
            for(let j = 0; j < 3; j++)
            {   
                if(this.#state[i][j] === null)
                {
                    fieldStr = fieldStr + 'n' + ' '
                    continue
                }
                fieldStr = fieldStr + this.#state[i][j] + ' '
            }

            fieldStr = fieldStr + '\n'
        }
        return fieldStr
    }

    setMode(newWalk) {
        this.whoWalks = newWalk
    }
    
    // метод для проверки свободна ли клетка или нет
    checkFreeCell(walkXY) {
        walkXY = walkXY.split(' ')
        if(this.#state[walkXY[0]][walkXY[1]] === null)
            return true
        else
            return false
    }

    // метод для понимания есть ли свободные ходы или нет
    noMoves() {
        for(let i = 0; i < 3; i++){
            for(let j = 0; j < 3; j++){
                if(this.#state[i][j] === null)
                    return false;
            }
        }
        return true;
    }

    FieldCellValue(walkXY) {

        if(this.checkFreeCell(walkXY)) {

            if(this.whoWalks === 'x') {
                walkXY = walkXY.split(' ')
                this.#state[walkXY[0]][walkXY[1]] = 'x'
                this.whoWalks = 'o'
            }

            else {
                walkXY = walkXY.split(' ')
                this.#state[walkXY[0]][walkXY[1]] = 'o'
                this.whoWalks = 'x'
            }
        }

        else {
            alert("Это поле уже занято! Введите другое");
            let gameWalk = prompt("Введите координаты поля (в вормате [X Y]")
            gameField.FieldCellValue(gameWalk);
        }
    }

    // метод для обхода поля
    checkField() {

        for(let i = 0; i < 3; i++)
        {
            if(this.#state[0][i] === this.#state[1][i] && this.#state[1][i] === this.#state[2][i] && this.#state[1][i] !== null)
                return this.#state[0][i];
            
            else if(this.#state[i][0] === this.#state[i][1] && this.#state[i][1] === this.#state[i][2] && this.#state[i][1] !== null)
                return this.#state[i][0];
    
            else if(this.#state[0][0] === this.#state[1][1] && this.#state[1][1] === this.#state[2][2] && this.#state[1][1] !== null)
                return this.#state[1][1];
    
            else if(this.#state[0][2] === this.#state[1][1] && this.#state[1][1] === this.#state[2][0] && this.#state[1][1] !== null)
                return this.#state[1][1];
        }
    
        return '-';
    };

    // метод для понимания найден ли победитель
    winnerIsFiend() {
        if(this.checkField() != '-')
            return true;
        else
            return false;
    }
    
    // метод вывода победителя
    printResult(value) {
        
        if(value === 'x')
        {
            alert('Крестики победили\n' + this.getGameFieldStatus());
            this.isOverGame = true;
        }
    
        else if(value === 'o')
        {
            alert('Нолики победили\n' + this.getGameFieldStatus());
            this.isOverGame = true;
        }
        
        else if(value === '-')
        {
            alert('Ничья\n' + this.getGameFieldStatus());
            this.isOverGame = true;
        }
    }
    
    // метод для поиска победителя
    findWiner() {
        this.printResult(this.checkField());
    } 

}

const gameField = new GameField();

while(!gameField.isOverGame) {
    alert("Актуальное поле: " + gameField.getGameFieldStatus() + '\n n - пустая клетка')
    alert("Ходят " + gameField.whoWalks + "!")
    let gameWalk = prompt("Введите координаты поля (в вормате [X Y])")
    gameField.FieldCellValue(gameWalk);
    if(gameField.noMoves() || gameField.winnerIsFiend())
        gameField.findWiner(); 
}


